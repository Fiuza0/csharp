﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Jogador j1 = new Jogador();
            Jogador j2 = new Jogador();
            Jogador j3 = new Jogador();
            Jogador j4 = new Jogador();

            j1.nome = "Rodrigo";
            j2.nome = "Jessica";
            j3.nome = "Fernando";
            j4.nome = "Gabriel";

            j1.jogada = Jogada.Pedra;
            j2.jogada = Jogada.Tesoura;
            j3.jogada = Jogada.Papel;
            j4.jogada = Jogada.Tesoura;

            List<string> jogss = new List<string>();
            jogss.Add(j1.jogada.ToString());
            jogss.Add(j2.jogada.ToString());
            jogss.Add(j3.jogada.ToString());
            jogss.Add(j4.jogada.ToString());

            List<Jogador> jogadores = new List<Jogador>();
            jogadores.Add(j1);
            jogadores.Add(j2);
            jogadores.Add(j3);
            jogadores.Add(j4);

            Console.WriteLine(CalcularPontos(jogss, jogadores));
            Console.ReadLine();
        }

        public static string CalcularPontos(List<string> jogadas, List<Jogador> jogadores)
        {
            string msg = "";
            foreach (var jogador in jogadores)
            {  
                foreach (var jogada in jogadas)
                {
                    if (jogador.jogada == Jogada.Tesoura)
                    {
                            if (jogada == "Pedra")
                            {
                                jogador.pontos -= 1;
                            }
                            else if(jogada == "Tesoura") { }
                            else
                            {
                                jogador.pontos += 1;
                            }
                        }
                    
                    else if (jogador.jogada == Jogada.Pedra)
                    {
                            if (jogada == "Papel")
                            {
                                jogador.pontos -= 1;
                            }
                        else if (jogada == "Pedra") { }
                        else
                            {
                                jogador.pontos += 1;
                            }
                        }
                    
                    else
                    {                      
                            if (jogada == "Tesoura")
                            {
                                jogador.pontos -= 1;
                            }
                        else if (jogada == "Papel") { }
                        else if (jogada == "Pedra")
                            {
                                jogador.pontos += 1;
                            }
                    }   
                }
                msg += jogador.nome + ": " + jogador.pontos + "\n";
            }
            return msg;
        }

        //ESTRUTURA ANTIGA

        //if (j1.jogada == Jogada.Tesoura)
        //{
        //    List<string> jogs = new List<string>();
        //    jogs.Add(j2.jogada.ToString());
        //    jogs.Add(j3.jogada.ToString());
        //    jogs.Add(j4.jogada.ToString());
        //    foreach (var jog in jogs)
        //    {
        //        if (jog == "Pedra")
        //        {
        //            j1.pontos -= 1;
        //        }
        //        else
        //        {
        //            j1.pontos += 1;
        //        }
        //    }
        //}
        //else if (j1.jogada == Jogada.Pedra)
        //{
        //    List<string> jogs = new List<string>();
        //    jogs.Add(j2.jogada.ToString());
        //    jogs.Add(j3.jogada.ToString());
        //    jogs.Add(j4.jogada.ToString());
        //    foreach (var jog in jogs)
        //    {
        //        if (jog == "Papel")
        //        {
        //            j1.pontos -= 1;
        //        }
        //        else
        //        {
        //            j1.pontos += 1;
        //        }
        //    }
        //}
        //else
        //{
        //    List<string> jogs = new List<string>();
        //    jogs.Add(j2.jogada.ToString());
        //    jogs.Add(j3.jogada.ToString());
        //    jogs.Add(j4.jogada.ToString());
        //    foreach (var jog in jogs)
        //    {
        //        if (jog == "Tesoura")
        //        {
        //            j1.pontos -= 1;
        //        }
        //        else
        //        {
        //            j1.pontos += 1;
        //        }
        //    }
        //}
        //if (j2.jogada == Jogada.Tesoura)
        //{
        //    List<string> jogs = new List<string>();
        //    jogs.Add(j1.jogada.ToString());
        //    jogs.Add(j3.jogada.ToString());
        //    jogs.Add(j4.jogada.ToString());
        //    foreach (var jog in jogs)
        //    {
        //        if (jog == "Pedra")
        //        {
        //            j2.pontos -= 1;
        //        }
        //        else
        //        {
        //            j2.pontos += 1;
        //        }
        //    }
        //}
        //else if (j2.jogada == Jogada.Pedra)
        //{
        //    List<string> jogs = new List<string>();
        //    jogs.Add(j1.jogada.ToString());
        //    jogs.Add(j3.jogada.ToString());
        //    jogs.Add(j4.jogada.ToString());
        //    foreach (var jog in jogs)
        //    {
        //        if (jog == "Papel")
        //        {
        //            j2.pontos -= 1;
        //        }
        //        else
        //        {
        //            j2.pontos += 1;
        //        }
        //    }
        //}
        //else
        //{
        //    List<string> jogs = new List<string>();
        //    jogs.Add(j1.jogada.ToString());
        //    jogs.Add(j2.jogada.ToString());
        //    jogs.Add(j4.jogada.ToString());
        //    foreach (var jog in jogs)
        //    {
        //        if (jog == "Tesoura")
        //        {
        //            j2.pontos -= 1;
        //        }
        //        else
        //        {
        //            j2.pontos += 1;
        //        }
        //    }
        //}
        //if (j3.jogada == Jogada.Tesoura)
        //{
        //    List<string> jogs = new List<string>();
        //    jogs.Add(j1.jogada.ToString());
        //    jogs.Add(j2.jogada.ToString());
        //    jogs.Add(j4.jogada.ToString());
        //    foreach (var jog in jogs)
        //    {
        //        if (jog == "Pedra")
        //        {
        //            j3.pontos -= 1;
        //        }
        //        else
        //        {
        //            j3.pontos += 1;
        //        }
        //    }
        //}
        //else if (j3.jogada == Jogada.Pedra)
        //{
        //    List<string> jogs = new List<string>();
        //    jogs.Add(j1.jogada.ToString());
        //    jogs.Add(j2.jogada.ToString());
        //    jogs.Add(j4.jogada.ToString());
        //    foreach (var jog in jogs)
        //    {
        //        if (jog == "Papel")
        //        {
        //            j3.pontos -= 1;
        //        }
        //        else
        //        {
        //            j3.pontos += 1;
        //        }
        //    }
        //}
        //else
        //{
        //    List<string> jogs = new List<string>();
        //    jogs.Add(j1.jogada.ToString());
        //    jogs.Add(j2.jogada.ToString());
        //    jogs.Add(j4.jogada.ToString());
        //    foreach (var jog in jogs)
        //    {
        //        if (jog == "Tesoura")
        //        {
        //            j3.pontos -= 1;
        //        }
        //        else
        //        {
        //            j3.pontos += 1;
        //        }
        //    }
        //}
        //if (j4.jogada == Jogada.Tesoura)
        //{
        //    List<string> jogs = new List<string>();
        //    jogs.Add(j1.jogada.ToString());
        //    jogs.Add(j2.jogada.ToString());
        //    jogs.Add(j3.jogada.ToString());
        //    foreach (var jog in jogs)
        //    {
        //        if (jog == "Pedra")
        //        {
        //            j4.pontos -= 1;
        //        }
        //        else
        //        {
        //            j4.pontos += 1;
        //        }
        //    }
        //}
        //else if (j4.jogada == Jogada.Pedra)
        //{
        //    List<string> jogs = new List<string>();
        //    jogs.Add(j1.jogada.ToString());
        //    jogs.Add(j2.jogada.ToString());
        //    jogs.Add(j3.jogada.ToString());
        //    foreach (var jog in jogs)
        //    {
        //        if (jog == "Papel")
        //        {
        //            j4.pontos -= 1;
        //        }
        //        else
        //        {
        //            j4.pontos += 1;
        //        }
        //    }
        //}
        //else
        //{
        //    List<string> jogs = new List<string>();
        //    jogs.Add(j1.jogada.ToString());
        //    jogs.Add(j2.jogada.ToString());
        //    jogs.Add(j3.jogada.ToString());
        //    foreach (var jog in jogs)
        //    {
        //        if (jog == "Tesoura")
        //        {
        //            j4.pontos -= 1;
        //        }
        //        else if (jog == "Pedra")
        //        {
        //            j4.pontos += 1;
        //        }
        //    }
        //}

        //Console.WriteLine(j1.nome + ":" +j1.pontos+"\n"+
        //                j2.nome + ":" + j2.pontos + "\n" +
        //                j3.nome + ":" + j3.pontos + "\n" +
        //                j4.nome + ":" + j4.pontos + "\n");
        //Console.ReadLine();
    }
}